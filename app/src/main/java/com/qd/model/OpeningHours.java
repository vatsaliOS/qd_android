package com.qd.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OpeningHours implements Serializable {

    private String day;
    private ArrayList<Timing> breakfast = new ArrayList<Timing>();
    private ArrayList<Timing> lunch = new ArrayList<Timing>();
    private ArrayList<Timing> dinner = new ArrayList<Timing>();

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public ArrayList<Timing> getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(Timing timing) {
        this.breakfast.add(timing);
    }

    public ArrayList<Timing> getLunch() {
        return lunch;
    }

    public void setLunch(Timing timing) {
        this.lunch.add(timing);
    }

    public ArrayList<Timing> getDinner() {
        return dinner;
    }

    public void setDinner(Timing timing) {
        this.dinner.add(timing);
    }
}
