package com.qd.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Restaurants implements Serializable {

    private int id;
    private String name;
    private String address;
    private String latitude;
    private String longitude;
    private String image;
    private String isJoined;
    private int slowmode;
    private int averageWaitingTime;
    private int distance;
    private ArrayList<OpeningHours> openingHourses = new ArrayList<OpeningHours>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getSlowmode() {
        return slowmode;
    }

    public void setSlowmode(int slowmode) {
        this.slowmode = slowmode;
    }

    public int getAverageWaitingTime() {
        return averageWaitingTime;
    }

    public void setAverageWaitingTime(int averageWaitingTime) {
        this.averageWaitingTime = averageWaitingTime;
    }

    public String getIsJoined() {
        return isJoined;
    }

    public void setIsJoined(String isJoined) {
        this.isJoined = isJoined;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public ArrayList<OpeningHours> getOpeningHourses() {
        return openingHourses;
    }

    public void setOpeningHourses(OpeningHours openingHours) {
        this.openingHourses.add(openingHours);
    }
}
