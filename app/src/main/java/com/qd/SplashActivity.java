package com.qd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.qd.interfaces.ConstantInterface;
import com.qd.login.LoginActivity;
import com.qd.restaurant.MainActivity;
import com.qd.util.ApplicationQd;
import com.qd.util.QdPreferences;

public class SplashActivity extends Activity implements ConstantInterface{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
                if (qdPreferences.getPreference(USER_ID, 0) != 0) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 500);

    }

}
