package com.qd.util;

import android.os.AsyncTask;
import com.qd.interfaces.OnTaskCompleteListener;
import java.io.File;
import java.util.HashMap;

public class CallApiByAsync extends AsyncTask<String, String, String> {
    private String url = "";
    private HashMap<String, String> textMap;
    private HashMap<String, File> textFile;
    private OnTaskCompleteListener onTaskCompleteListener;

    public CallApiByAsync(String mUrl, HashMap<String, String> mParms, HashMap<String, File> mFiles, OnTaskCompleteListener onTaskCompleteListener) {
        this.textMap = mParms;
        this.textFile = mFiles;
        this.url = mUrl;
        this.onTaskCompleteListener = onTaskCompleteListener;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        GetJsonUrl mGetJsonUrl = new GetJsonUrl();
        String response = mGetJsonUrl.getJSONResponseFromUrl(url, textMap, textFile);
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if (result != null && !result.equals("")) {
            onTaskCompleteListener.onComplete(result);
        }
    }

}
