package com.qd.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.widget.EditText;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.qd.R;
import com.qd.interfaces.ConstantInterface;

public class AppUtility implements ConstantInterface{

    private ProgressDialog dialog;


    // isValidEmail : return String if email address is valid or null if not
    public boolean isValidEmail(EditText edtEmail) {

        if (edtEmail.getText().toString() == null || edtEmail.getText().toString().length() == 0) {

            return false;

        } else if (Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString()).matches() == false) {

            return false;

        } else {

            return true;

        }

    }

    // dpToPixel : returns px converted by dp
    public int dpToPixel(Context context, float dp) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;

    }

    // showProgressDialog : open dialog to show progress

    public void showProgressDialog(Activity activity, String message) {

        dialog = new ProgressDialog(activity);
        dialog.setIndeterminate(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        //dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.show();

    }

    // hideProgressDialog : dismiss dialog if already open

    public void hideProgressDialog() {

        if (dialog.isShowing()) {

            dialog.dismiss();

        }

    }

    //isInternetEnable : return true if Internet is enable or false if not

    public boolean isInternetEnable(Context context) {

        Activity activity = (Activity) context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null) {

            showOkDialog(context, "Your Internet connection is not enable.");
            return false;

        } else {

            return activeNetworkInfo != null;

        }

    }

    // showOkDialog : It's a Warning or Information dialog.

    public void showOkDialog(Context context, String message) {

        AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(context.getResources().getString(R.string.app_name));
        adb.setMessage(message);
        adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        if (!((Activity) context).isFinishing() && message.length() > 0) {

            adb.show();

        }

    }

    // Check weather google play services is available or not.
    public boolean checkPlayServices(Context context) {

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();

        int result = googleAPI.isGooglePlayServicesAvailable(context);

        if (result != ConnectionResult.SUCCESS) {

            if (googleAPI.isUserResolvableError(result)) {

                googleAPI.getErrorDialog((Activity) context, result, PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }

            return false;
        }

        return true;

    }

}
