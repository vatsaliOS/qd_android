package com.qd.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.qd.interfaces.ConstantInterface;

public class ApplicationQd extends Application implements ConstantInterface {

    static ApplicationQd instance = null;
    private Context context;
    private QdPreferences qdPreferences;
    private AppUtility appUtility;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = getApplicationContext();
        qdPreferences = new QdPreferences(context);
        appUtility = new AppUtility();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread thread, Throwable e) {

                handleUncaughtException(thread, e);

            }

        });

    }

    public AppUtility getAppUtility() {

        return appUtility;
    }

    public QdPreferences getSharedPreferences() {

        return qdPreferences;
    }

    public static ApplicationQd getInstance() {
        return instance;
    }

    @Override
    public void onTerminate() {

        super.onTerminate();

    }

    public void handleUncaughtException(Thread thread, Throwable e) {

        // not all Android versions will print the stack trace automatically
        e.printStackTrace();
        Log.d(TAG, "Uncaught Exception found!!!");

    }

}
