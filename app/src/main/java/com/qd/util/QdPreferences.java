package com.qd.util;

import android.content.Context;
import android.content.SharedPreferences;

public class QdPreferences {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public QdPreferences(Context context) {

        sharedPreferences = context.getApplicationContext().getSharedPreferences("client_prefs", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    public void clearPreferences() {

        editor.clear();

    }

    public void setPreference(String key, String value) {

        editor.putString(key, value);
        editor.commit();

    }

    public void setPreference(String key, int value) {

        editor.putInt(key, value);
        editor.commit();

    }

    public void setPreference(String key, boolean value) {

        editor.putBoolean(key, value);
        editor.commit();

    }

    public void setPreference(String key, float value) {

        editor.putFloat(key, value);
        editor.commit();

    }

    public void setPreference(String key, long value) {

        editor.putLong(key, value);
        editor.commit();

    }

    public String getPreference(String key, String value) {

        return sharedPreferences.getString(key, value);

    }

    public int getPreference(String key, int value) {

        return sharedPreferences.getInt(key, value);

    }

    public boolean getPreference(String key, boolean value) {

        return sharedPreferences.getBoolean(key, value);

    }

    public float getPreference(String key, float value) {

        return sharedPreferences.getFloat(key, value);

    }

    public float getPreference(String key, long value) {

        return sharedPreferences.getLong(key, value);

    }

}
