package com.qd.notification;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.qd.R;
import com.qd.customcontrols.LoadMoreListView;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.model.OpeningHours;
import com.qd.model.Restaurants;
import com.qd.model.Timing;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

// Created by sotsys072 on 2/1/16.

public class NotifSetting extends Activity implements View.OnClickListener, ConstantInterface {

    private TextView textHeader;
    private TextView textBack;
    private LoadMoreListView lvRestoName;
    private RestoAdapter restoAdapter;
    private AppUtility appUtility;
    private int pageNumber = 1;
    private ArrayList<Restaurants> restaurantses;
    private TextView textNoData;
    private SearchView svSearch;
    private QdPreferences qdPreferences;
    private boolean isSearching;
    private String searchText;
    private boolean isPageUpdated;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif_setting);
        initControls();

        getSlowTimeVenue("");


    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("isPageUpdated", isPageUpdated);
        setResult(RESULT_OK, intent);
        finish();

    }

    // Initializing controls
    private void initControls() {
        qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getResources().getString(R.string.text_setting));
        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        lvRestoName = (LoadMoreListView) findViewById(R.id.lvRestoName);
        lvRestoName.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            public void onLoadMore() {
                if (isSearching) {
                    getSlowTimeVenue(searchText);
                } else {
                    getSlowTimeVenue("");
                }
            }
        });
        svSearch = (SearchView) findViewById(R.id.svSearch);

        svSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                restaurantses.clear();
                pageNumber = 1;
                getSlowTimeVenue("");
                return false;
            }
        });
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                svSearch.clearFocus();
                searchText = query;
                isSearching = true;
                restaurantses.clear();
                pageNumber = 1;
                getSlowTimeVenue(searchText);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        restaurantses = new ArrayList<Restaurants>();
        restoAdapter = new RestoAdapter();
        lvRestoName.setAdapter(restoAdapter);
        appUtility = ApplicationQd.getInstance().getAppUtility();
        textNoData = (TextView) findViewById(R.id.textNoData);
    }

    // Api call to get Slow Time user.
    private void getSlowTimeVenue(String searchText) {
        if (appUtility.isInternetEnable(this)) {

            if (pageNumber == 1) {
                appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));
            }

            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("user_id", "" + qdPreferences.getPreference(USER_ID, -1));
            hashMap.put("page", "" + pageNumber++);

            if (!searchText.equals("")) {
                hashMap.put("query", "" + searchText);
            }
            new CallApiByAsync(BASE_URL + (searchText.equals("") ? GET_SLOW_TIME_ALERTS_VENUES : SEARCH_SLOW_TIME_ALERTS_VENUES), hashMap, null, new OnTaskCompleteListener() {
                @Override
                public void onComplete(String resultString) {
                    Log.d("log_tag", "testAPI response >> " + resultString);

                    if (!resultString.equals("Error")) {
                        try {

                            JSONObject jsonObject = new JSONObject(resultString);

                            if (jsonObject.getInt("code") == 200) {

                                JSONArray jsonArray = jsonObject.getJSONArray("venues");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Restaurants restaurants = new Restaurants();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    restaurants.setId(jsonObject1.getInt("id"));
                                    restaurants.setName(jsonObject1.getString("name"));
                                    restaurants.setAddress(jsonObject1.getString("address"));
                                    restaurants.setLatitude(jsonObject1.getString("latitude"));
                                    restaurants.setLongitude(jsonObject1.getString("longitude"));
                                    restaurants.setImage(jsonObject1.getString("image"));
                                    restaurants.setSlowmode(jsonObject1.getInt("slowmode"));
                                    restaurants.setAverageWaitingTime(jsonObject1.getInt("average_waiting_time"));

                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("opening_hours");

                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        OpeningHours openingHours = new OpeningHours();
                                        JSONObject jsonObject2 = jsonArray1.getJSONObject(j);

                                        openingHours.setDay(jsonObject2.getString("day"));

                                        Timing timing = new Timing();
                                        JSONObject jsonObject3 = jsonObject2.getJSONObject("Breakfast");
                                        timing.setStartTime(jsonObject3.getString("start"));
                                        timing.setEndTime(jsonObject3.getString("end"));
                                        openingHours.setBreakfast(timing);

                                        JSONObject jsonObject4 = jsonObject2.getJSONObject("Lunch");
                                        timing.setStartTime(jsonObject4.getString("start"));
                                        timing.setEndTime(jsonObject4.getString("end"));
                                        openingHours.setLunch(timing);

                                        JSONObject jsonObject5 = jsonObject2.getJSONObject("Dinner");
                                        timing.setStartTime(jsonObject5.getString("start"));
                                        timing.setEndTime(jsonObject5.getString("end"));
                                        openingHours.setDinner(timing);

                                        restaurants.setOpeningHourses(openingHours);

                                    }

                                    restaurantses.add(restaurants);

                                }
                            }

                            if (restaurantses.size() == 0) {

                                textNoData.setVisibility(View.VISIBLE);
                                lvRestoName.setVisibility(View.GONE);

                            } else {
                                textNoData.setVisibility(View.GONE);
                                lvRestoName.setVisibility(View.VISIBLE);
                            }
                            onItemsLoadComplete();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    // Notify adapter, dismiss loader, stop refresh animation
    private void onItemsLoadComplete() {

        restoAdapter.notifyDataSetChanged();
        appUtility.hideProgressDialog();
        lvRestoName.onLoadMoreComplete();

    }

    // Api call to set slow time mode.
    private void setSlowMode(final int position) {

        qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("userslowtimealert[restaurant_id]", "" + restaurantses.get(position).getId());
        hashMap.put("userslowtimealert[user_id]", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("userslowtimealert[is_show_time_alert]", restaurantses.get(position).getSlowmode() == 1 ? "" + 0 : "" + 1);


        new CallApiByAsync(BASE_URL + UPDATE_SLOW_TIME_ALERT, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        if (jsonObject.getInt("code") == 200) {

                            isPageUpdated = true;
                            if (restaurantses.get(position).getSlowmode() == 1) {
                                restaurantses.get(position).setSlowmode(0);
                            } else {
                                restaurantses.get(position).setSlowmode(1);
                            }
                            restoAdapter.notifyDataSetChanged();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textBack:

                onBackPressed();
                break;

            default:
                break;

        }

    }

    // List Adapter
    public class RestoAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        public RestoAdapter() {

            inflater = getLayoutInflater();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_setting, null);

                holder.imgResto = (ImageView) convertView.findViewById(R.id.imgResto);
                holder.textRestoName = (TextView) convertView.findViewById(R.id.textRestoName);
                holder.textAddress = (TextView) convertView.findViewById(R.id.textAddress);
                holder.imgToggle = (ImageView) convertView.findViewById(R.id.imgToggle);

                convertView.setTag(holder);

            } else {

                holder = (ViewHolder) convertView.getTag();

            }

            holder.textRestoName.setText(restaurantses.get(position).getName());
            holder.textAddress.setText(restaurantses.get(position).getAddress());
            Picasso.with(NotifSetting.this).load(restaurantses.get(position).getImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.imgResto);

            if (restaurantses.get(position).getSlowmode() == 1) {
                holder.imgToggle.setImageResource(R.drawable.img_on);
            } else {
                holder.imgToggle.setImageResource(R.drawable.img_off);
            }

            holder.imgToggle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSlowMode(position);
                }
            });

            return convertView;

        }

        public class ViewHolder {

            private TextView textRestoName, textAddress;
            private ImageView imgResto, imgToggle;

        }

        public int getCount() {

            return restaurantses.size();

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }

}
