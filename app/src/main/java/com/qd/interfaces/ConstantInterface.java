package com.qd.interfaces;

/**
 * Created by sotsys072 on 27/8/15.
 */
public interface ConstantInterface {

    // App Api's
    String BASE_URL = "http://main.spaceotechnologies.com/qd/api/v1/";
    String SIGN_UP = "auth/signup";
    String SIGN_IN = "auth/signin";
    String VERIFICATION_CODE = "auth/verificationcode";
    String RESEND_VERIFICATION_CODE = "auth/resend_verification_code";
    String GETVENUES = "users/getvenues/";
    String SEARCH = "users/search/";
    String UPDATE_SLOW_TIME_ALERT = "restaurants/update_slow_time_alerts/";
    String GET_SLOW_TIME_ALERTS_VENUES = "restaurants/get_slow_time_alerts_venues/";
    String SEARCH_SLOW_TIME_ALERTS_VENUES = "restaurants/search_slow_time_alerts_venues/";
    String JOIN_QUEUE = "restaurants/join_queue/";
    // End of App Api's

    // Permission Variable Start
    int PERMISSION_REQUEST_LOCATION = 101;
        /*RANGE
        startActivityForResult() in FragmentActivity requires the requestCode to be of 16 bits, meaning the range is from 0 to 65535.

        Also, validateRequestPermissionsRequestCode in FragmentActivity requires requestCode to be of 8 bits, meaning the range is from 0 to 255.*/
    // Permission Variable End

    // General
    String TAG = "log_tag";
    String ACCESS_TOKEN = "access_token";
    String NAV_ITEM_ID = "navItemId";
    int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    int UPDATE_INTERVAL = 10000; // 10 sec
    int FATEST_INTERVAL = 5000; // 5 sec
    int DISPLACEMENT = 10; // 10 meters
    long DRAWER_CLOSE_DELAY_MS = 350;
    String[] days = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    String[] foodTime = {"Breakfast", "Lunch", "Dinner"};

    // Preference Statics
    String USER_ID = "user_id";
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String EMAIL = "email";
    String PHONE = "phone";
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";
    String PAGE = "page";
    String VERIFIED = "verified";
    String WHICHPAGE = "whichPage";

}
