package com.qd.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTVMedium extends TextView {

    public CustomTVMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTVMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTVMedium(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "HindVadodara-Medium.ttf");
        setTypeface(typeface, 1);

    }

}