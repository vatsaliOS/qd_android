package com.qd.customcontrols;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTVRegular extends TextView {

    public CustomTVRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTVRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTVRegular(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "HindVadodara-Regular.ttf");
        setTypeface(typeface, 1);

    }

}