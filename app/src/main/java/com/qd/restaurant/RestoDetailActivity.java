package com.qd.restaurant;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qd.R;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.model.OpeningHours;
import com.qd.model.Restaurants;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class RestoDetailActivity extends FragmentActivity implements ConstantInterface, OnMapReadyCallback, View.OnClickListener {

    private SupportMapFragment mapFragment;
    private TextView textHeader;
    private TextView textJoin;
    private TextView textBack;
    private TextView textAvgWait;
    private TextView textRestoName;
    private TextView textAddress;
    private TextView textMonST;
    private TextView textMonET;
    private TextView textTueST;
    private TextView textTueET;
    private TextView textWedST;
    private TextView textWedET;
    private TextView textThuST;
    private TextView textThuET;
    private TextView textFriST;
    private TextView textFriET;
    private TextView textSatST;
    private TextView textSatET;
    private TextView textSunST;
    private TextView textSunET;
    private ImageView imgSlowMode;
    private ImageView imgLogo;
    private AppUtility appUtility;
    private Restaurants restaurants;
    private ArrayList<OpeningHours> openingHourses;
    private String latitude;
    private String longitude;
    private boolean slowModeFlag;
    private int position;
    private QdPreferences qdPreferences;

    private ListView lvTimings;
    private TimeAdapter timeAdapter;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_detail);
        initControls();

        // No need to check any condition as Developer is sure that the bundle can not be blank.
        Bundle bundle = getIntent().getExtras();
        restaurants = (Restaurants) bundle.getSerializable("resto_info");
        openingHourses = restaurants.getOpeningHourses();
        position = bundle.getInt("position");
        setValues();
    }

    @Override
    public void onBackPressed() {
        qdPreferences.setPreference(WHICHPAGE, "Restro Detail Page");
        Intent intent = new Intent();
        intent.putExtra("slowModeFlag", slowModeFlag);
        intent.putExtra("position", position);
        intent.putExtra("isJoined", restaurants.getIsJoined());
        setResult(RESULT_OK, intent);
        finish();

        //super.onBackPressed();
    }

    // Initializing controls
    private void initControls() {

        qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
        params.height = (metrics.heightPixels / 2);
        mapFragment.getView().setLayoutParams(params);
        textHeader = (TextView) findViewById(R.id.textHeader);
        textJoin = (TextView) findViewById(R.id.textJoin);
        textJoin.setOnClickListener(this);
        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        textAvgWait = (TextView) findViewById(R.id.textAvgWait);
        textRestoName = (TextView) findViewById(R.id.textRestoName);
        textAddress = (TextView) findViewById(R.id.textAddress);
        textAddress.setMovementMethod(new ScrollingMovementMethod());
        imgSlowMode = (ImageView) findViewById(R.id.imgSlowMode);
        imgSlowMode.setOnClickListener(this);
        imgLogo = (ImageView) findViewById(R.id.imgLogo);
        lvTimings = (ListView) findViewById(R.id.lvTimings);
        timeAdapter = new TimeAdapter();
        lvTimings.setAdapter(timeAdapter);

        appUtility = ApplicationQd.getInstance().getAppUtility();

    }

    // List Adapter
    public class TimeAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        public TimeAdapter() {

            inflater = getLayoutInflater();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_timing, null);

                holder.textDay = (TextView) convertView.findViewById(R.id.textDay);
                holder.textBreakFast = (TextView) convertView.findViewById(R.id.textBreakFast);
                holder.textLunch = (TextView) convertView.findViewById(R.id.textLunch);
                holder.textDinner = (TextView) convertView.findViewById(R.id.textDinner);

                convertView.setTag(holder);

            } else {

                holder = (ViewHolder) convertView.getTag();

            }

            holder.textDay.setText(days[position]);
            holder.textBreakFast.setText(openingHourses.get(position).getBreakfast().get(0).getStartTime() + " - " + openingHourses.get(position).getBreakfast().get(0).getEndTime());
            holder.textLunch.setText(openingHourses.get(position).getLunch().get(0).getStartTime() + " - " + openingHourses.get(position).getLunch().get(0).getEndTime());
            holder.textDinner.setText(openingHourses.get(position).getDinner().get(0).getStartTime() + " - " + openingHourses.get(position).getDinner().get(0).getEndTime());

            return convertView;

        }

        public class ViewHolder {

            private TextView textDay, textBreakFast, textLunch, textDinner;

        }

        public int getCount() {

            return 7;

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }

    // Api call to set Slow mode.
    private void setSlowMode() {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("userslowtimealert[restaurant_id]", "" + restaurants.getId());
        hashMap.put("userslowtimealert[user_id]", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("userslowtimealert[is_show_time_alert]", slowModeFlag == true ? "" + 0 : "" + 1);

        new CallApiByAsync(BASE_URL + UPDATE_SLOW_TIME_ALERT, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        if (jsonObject.getInt("code") == 200) {

                            if (slowModeFlag) {
                                imgSlowMode.setImageResource(R.drawable.img_off);
                                slowModeFlag = false;
                            } else {
                                imgSlowMode.setImageResource(R.drawable.img_on);
                                slowModeFlag = true;
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // Api call to join queue.
    private void joinQueue(String number) {
        appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("joinqueue[user_id]", "" + qdPreferences.getPreference(USER_ID, -1));
        hashMap.put("joinqueue[restaurant_id]", "" + restaurants.getId());
        hashMap.put("joinqueue[number_of_people]", number);

        new CallApiByAsync(BASE_URL + JOIN_QUEUE, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        if (jsonObject.getInt("code") == 200) {

                            restaurants.setIsJoined("1");
                            textJoin.setText("Already");
                            appUtility.hideProgressDialog();
                            Intent intent = new Intent(RestoDetailActivity.this, JoinRestoActivity.class);
                            intent.putExtra("result", resultString);
                            startActivity(intent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // Map initialization
    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng latLng = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        googleMap.addMarker(new MarkerOptions().position(latLng).title(restaurants.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f));

    }

    // Setting values of restaurant.
    private void setValues() {
        textHeader.setText(restaurants.getName());
        textAvgWait.setText(restaurants.getAverageWaitingTime() + " min average wait");
        textRestoName.setText(restaurants.getName());
        textAddress.setText(restaurants.getAddress());
        if (restaurants.getIsJoined().equals("1")) {
            textJoin.setText("Already");
        } else {
            textJoin.setText(getResources().getString(R.string.text_join_queue));
        }
        latitude = restaurants.getLatitude();
        longitude = restaurants.getLongitude();
        Picasso.with(this).load(restaurants.getImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imgLogo);
        if (restaurants.getSlowmode() == 0) {
            imgSlowMode.setImageResource(R.drawable.img_off);
            slowModeFlag = false;
        } else {
            imgSlowMode.setImageResource(R.drawable.img_on);
            slowModeFlag = true;
        }
        mapFragment.getMapAsync(this);
    }

    // A method to add people in the queue to the restaurants.
    private void addPeople() {

        final Dialog dialog = new Dialog(RestoDetailActivity.this);
        dialog.setContentView(R.layout.dialog_join_queue);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        final EditText edtPeople = (EditText) dialog.findViewById(R.id.edtPeople);
        edtPeople.setInputType(InputType.TYPE_CLASS_NUMBER);
        TextView textOk = (TextView) dialog.findViewById(R.id.textOk);
        TextView textCancel = (TextView) dialog.findViewById(R.id.textCancel);
        textOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtPeople.getText().toString().equals("")) {
                    joinQueue(edtPeople.getText().toString());
                }
                dialog.dismiss();
            }
        });
        textCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.textJoin:

                if (restaurants.getIsJoined().equals("1")) {
                    joinQueue("0");
                } else {
                    addPeople();
                }

                break;
            case R.id.textBack:
                onBackPressed();
                break;
            case R.id.imgSlowMode:
                if (appUtility.isInternetEnable(this)) {
                    setSlowMode();
                }
                break;
            default:
                break;
        }

    }
}
