package com.qd.restaurant;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qd.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class JoinRestoActivity extends Activity implements View.OnClickListener {

    private LinearLayout llnotify;
    private TextView textBack;
    private TextView textName;
    private TextView textEstTime;
    private TextView textQueue;
    private ImageView imgResto;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_resto);
        initControls();

        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey("result")) {
            setValues(bundle.getString("result"));
        }

    }

    // Initializing controls
    private void initControls() {

        llnotify = (LinearLayout) findViewById(R.id.llNotify);
        llnotify.setOnClickListener(this);
        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        textName = (TextView) findViewById(R.id.textName);
        textEstTime = (TextView) findViewById(R.id.textEstTime);
        textQueue = (TextView) findViewById(R.id.textQueue);
        imgResto = (ImageView) findViewById(R.id.imgResto);

    }

    // setting page values
    private void setValues(String resultString) {

        try {

            JSONObject jsonObject = new JSONObject(resultString).getJSONObject("queue_details");

            textName.setText(jsonObject.getString("name"));
            textEstTime.setText(jsonObject.getString("estimate_wait_time") + " minutes");
            textQueue.setText(jsonObject.getString("in_the_queue"));
            Picasso.with(JoinRestoActivity.this).load(jsonObject.getString("image")).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(imgResto);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textBack:

                onBackPressed();
                break;

            case R.id.llNotify:
                onBackPressed();
                break;

            default:
                break;

        }

    }

}
