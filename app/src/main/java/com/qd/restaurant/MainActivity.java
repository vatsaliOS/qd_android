package com.qd.restaurant;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qd.R;
import com.qd.customcontrols.LoadMoreListView;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.login.LoginActivity;
import com.qd.notification.NotifSetting;
import com.qd.model.OpeningHours;
import com.qd.model.Restaurants;
import com.qd.model.Timing;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends FragmentActivity implements ConstantInterface, NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ActivityCompat.OnRequestPermissionsResultCallback, LocationListener {


    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    //private int mNavItemId;
    private ImageView imgMenu;
    private Context context;
    private LoadMoreListView lvRestoName;
    private RestoAdapter restoAdapter;
    private SupportMapFragment mapFragment;
    private TextView textFlip;
    private boolean flipFlag;
    private SearchView svSearch;
    private View fragmentMap;
    private AppUtility appUtility;
    private ArrayList<Restaurants> restaurantses;
    private QdPreferences qdPreferences;
    private TextView textNoData;
    private View mLayout;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private double currLat;
    private double currLong;
    private int pageNumber = 1;
    private float distance = 0;
    private boolean isFirstTime;
    private boolean isSearchOn;
    private boolean isLocationRequesting;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initControls();

        /*if (null == savedInstanceState) {

            mNavItemId = R.id.notification_setting;

        } else {

            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);

        }*/

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

    }

    /*@Override
    protected void onSaveInstanceState(final Bundle outState) {

        super.onSaveInstanceState(outState);
        //outState.putInt(NAV_ITEM_ID, mNavItemId);

    }*/

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient != null) {
            isLocationRequesting = true;
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, MainActivity.this);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLocationRequesting) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, MainActivity.this);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isLocationRequesting = false;
        if (requestCode == 102 && resultCode == RESULT_OK) {

            if (data.getExtras().getBoolean("isPageUpdated")) {

                isFirstTime = true;
                checkPermission();

            }

        } else if (requestCode == 101 && resultCode == RESULT_OK) {

            int position = data.getExtras().getInt("position");
            if (data.getExtras().getBoolean("slowModeFlag", false)) {
                restaurantses.get(position).setSlowmode(1);
            } else {
                restaurantses.get(position).setSlowmode(0);
            }
            if (data.getExtras().getString("isJoined").equals("1")) {
                restaurantses.get(position).setIsJoined("1");
            } else {
                restaurantses.get(position).setIsJoined("0");
            }
            restoAdapter.notifyDataSetChanged();
            checkPermission();

        } else if (requestCode == 1000 && resultCode == RESULT_CANCELED) {

            Toast.makeText(context, getResources().getString(R.string.location_off), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            getLocation();
        }

    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {

            mDrawerLayout.closeDrawer(GravityCompat.START);

        } else {

            super.onBackPressed();

        }

    }

    // Initializing controls
    private void initControls() {

        appUtility = ApplicationQd.getInstance().getAppUtility();
        context = MainActivity.this;
        if (!appUtility.checkPlayServices(context)) {
            finish();
        }
        buildGoogleApiClient();
        isFirstTime = true;
        mLayout = findViewById(R.id.drawer_layout);
        qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        imgMenu = (ImageView) findViewById(R.id.imgMenu);
        imgMenu.setOnClickListener(this);
        lvRestoName = (LoadMoreListView) findViewById(R.id.lvRestoName);

        lvRestoName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, RestoDetailActivity.class);
                intent.putExtra("resto_info", restaurantses.get(position));
                intent.putExtra("position", position);
                startActivityForResult(intent, 101);

            }
        });

        lvRestoName.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            public void onLoadMore() {

                pageNumber++;
                Log.d(TAG, "1");
                getAllVenue("");
            }
        });

        restaurantses = new ArrayList<Restaurants>();
        restoAdapter = new RestoAdapter();
        lvRestoName.setAdapter(restoAdapter);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        fragmentMap = (View) findViewById(R.id.map);
        setMapHeight(true);
        textFlip = (TextView) findViewById(R.id.textFlip);
        textFlip.setOnClickListener(this);
        svSearch = (SearchView) findViewById(R.id.svSearch);
        svSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                fragmentMap.setVisibility(View.VISIBLE);
                if (flipFlag) {

                    flipFlag = true;
                    textFlip.setText(getResources().getString(R.string.text_list));
                    setMapHeight(false);
                    //lvRestoName.setVisibility(View.GONE);

                } else {

                    flipFlag = false;
                    textFlip.setText(getResources().getString(R.string.text_map));
                    setMapHeight(true);
                    //lvRestoName.setVisibility(View.VISIBLE);

                }
                if (isSearchOn) {
                    getAllVenue("");
                    isSearchOn = false;
                }
                return false;
            }
        });
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextChange(String newText) {
                if (fragmentMap.getVisibility() == View.VISIBLE && (!newText.isEmpty())) {
                    fragmentMap.setVisibility(View.GONE);
                    lvRestoName.setVisibility(View.VISIBLE);
                }

                return false;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {

                if (!query.equals("")) {
                    svSearch.clearFocus();
                    isSearchOn = true;
                    restaurantses.clear();
                    pageNumber = 1;
                    getAllVenue(query);
                }
                return false;

            }

        });
        textNoData = (TextView) findViewById(R.id.textNoData);

    }

    // Api call to get venue list.
    private void getAllVenue(String query) {

        if (appUtility.isInternetEnable(this)) {
            if (pageNumber == 1) {
                appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));
            }

            QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

            HashMap<String, String> hashMap = new HashMap<String, String>();
            qdPreferences.setPreference(LATITUDE, "" + currLat);
            qdPreferences.setPreference(LONGITUDE, "" + currLong);

            hashMap.put("latitude", "" + currLat);
            hashMap.put("longitude", "" + currLong);
            hashMap.put("page", "" + pageNumber);
            hashMap.put("user_id", "" + qdPreferences.getPreference(USER_ID, -1));

            if (!query.equals("")) {
                hashMap.put("query", query);
            }

            new CallApiByAsync(BASE_URL + (query.equals("") ? GETVENUES : SEARCH), hashMap, null, new OnTaskCompleteListener() {
                @Override
                public void onComplete(String resultString) {
                    Log.d("log_tag", "testAPI response >> " + resultString);

                    if (!resultString.equals("Error")) {
                        try {

                            JSONObject jsonObject = new JSONObject(resultString);
                            if (jsonObject.getInt("code") == 200) {
                                JSONArray jsonArray = jsonObject.getJSONArray("venues");

                                if (pageNumber == 1) {
                                    restaurantses.clear();
                                }

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Restaurants restaurants = new Restaurants();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    restaurants.setId(jsonObject1.getInt("id"));
                                    restaurants.setName(jsonObject1.getString("name"));
                                    restaurants.setAddress(jsonObject1.getString("address"));
                                    restaurants.setLatitude(jsonObject1.getString("latitude"));
                                    restaurants.setLongitude(jsonObject1.getString("longitude"));
                                    restaurants.setImage(jsonObject1.getString("image"));
                                    restaurants.setIsJoined(jsonObject1.getString("is_joined"));
                                    restaurants.setSlowmode(jsonObject1.getInt("slowmode"));
                                    restaurants.setAverageWaitingTime(jsonObject1.getInt("average_waiting_time"));
                                    restaurants.setDistance(jsonObject1.getInt("distance"));

                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("opening_hours");

                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        OpeningHours openingHours = new OpeningHours();
                                        JSONObject jsonObject2 = jsonArray1.getJSONObject(j);

                                        openingHours.setDay(jsonObject2.getString("day"));

                                        for (int k = 0; k < 3; k++) {
                                            JSONObject jsonObject3 = jsonObject2.getJSONObject(foodTime[k]);
                                            Timing timing = new Timing();
                                            timing.setStartTime((jsonObject3.getString("start").contains(":00") ? jsonObject3.getString("start").replace(":00", "") : jsonObject3.getString("start")).replace(" ", ""));
                                            timing.setEndTime((jsonObject3.getString("end").contains(":00") ? jsonObject3.getString("end").replace(":00", "") : jsonObject3.getString("end")).replace(" ", ""));
                                            switch (k) {
                                                case 0:
                                                    openingHours.setBreakfast(timing);
                                                    break;
                                                case 1:
                                                    openingHours.setLunch(timing);
                                                    break;
                                                case 2:
                                                    openingHours.setDinner(timing);
                                                    break;
                                                default:
                                                    break;
                                            }

                                        }

                                        restaurants.setOpeningHourses(openingHours);

                                    }

                                    restaurantses.add(restaurants);

                                }
                            }

                            if (restaurantses.size() == 0) {

                                textNoData.setVisibility(View.VISIBLE);
                                lvRestoName.setVisibility(View.GONE);

                            } else {

                                textNoData.setVisibility(View.GONE);
                                lvRestoName.setVisibility(View.VISIBLE);

                            }
                            onItemsLoadComplete();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //appUtility.showProgressDialog(this, getResources().getString(R.string.text_location));
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, MainActivity.this);
        }
    }

    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();

    }

    // Checking whether location service is enable or not.
    public void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        // Creating location request object
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FATEST_INTERVAL);
        locationRequest.setSmallestDisplacement(DISPLACEMENT);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e("TAG", "LocationActive");
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        Log.e("TAG", "Not Active");
                        break;
                }
            }
        });
    }

    // Comparing b/w old latlong and current latlong
    private void compareLocation() {
        Location locationA = new Location("Location A");
        locationA.setLatitude(Double.parseDouble(qdPreferences.getPreference(LATITUDE, "")));
        locationA.setLongitude(Double.parseDouble(qdPreferences.getPreference(LONGITUDE, "")));
        Location locationB = new Location("Location B");
        locationB.setLatitude(currLat);
        locationB.setLongitude(currLong);

        distance = locationA.distanceTo(locationB); // In meters

        if (distance > 500 || isFirstTime) {
            isFirstTime = false;
            pageNumber = 1;
            getAllVenue("");
        }
    }

    // Google api interface abstract methods start
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        Log.d(TAG, "On Connect");
        checkPermission();

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    // Location Listener interface abstract method start
    @Override
    public void onLocationChanged(Location location) {
        //restaurantses.clear();
        currLat = location.getLatitude();
        currLong = location.getLongitude();
        //appUtility.hideProgressDialog();

        compareLocation();
    }

    // Google map initialization
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currLat, currLong), 15f));

        }

        for (int i = 0; i < restaurantses.size(); i++) {
            Log.d(TAG, "lat = " + restaurantses.get(i).getLatitude() + " Long = " + restaurantses.get(i).getLongitude());
            LatLng latLng = new LatLng(Double.parseDouble(restaurantses.get(i).getLatitude()), Double.parseDouble(restaurantses.get(i).getLongitude()));
            googleMap.addMarker(new MarkerOptions().position(latLng).title(restaurantses.get(i).getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.img_pin)));
        }

    }

    // checking Runtime permission
    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationChecker(googleApiClient, MainActivity.this);

        } else {
            requestLocationPermission();
        }
    }

    // Requesting Runtime permission
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            AlertDialog.Builder alBuilder = new android.app.AlertDialog.Builder(this);
            alBuilder.setTitle(getResources().getString(R.string.app_name));
            alBuilder.setMessage(getResources().getString(R.string.need_loc_permission));
            alBuilder.setCancelable(false);
            alBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
                }
            });
            alBuilder.show();

        } else {
            Snackbar.make(mLayout, getResources().getString(R.string.request_loc_permission), Snackbar.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Snackbar.make(mLayout, getResources().getString(R.string.permission_granted), Snackbar.LENGTH_SHORT).show();
                locationChecker(googleApiClient, MainActivity.this);
            } else {
                Toast.makeText(context, getResources().getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // Navigation view Click listener
    private void navigate(final int itemId) {

        Intent intent = null;

        switch (itemId) {

            case R.id.notification_setting:

                intent = new Intent(MainActivity.this, NotifSetting.class);
                startActivityForResult(intent, 102);
                break;

            case R.id.sign_out:

                QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
                qdPreferences.setPreference(USER_ID, 0);
                qdPreferences.setPreference(FIRST_NAME, "");
                qdPreferences.setPreference(LAST_NAME, "");
                qdPreferences.setPreference(EMAIL, "");
                qdPreferences.setPreference(PHONE, "");
                qdPreferences.setPreference(LATITUDE, "");
                qdPreferences.setPreference(LONGITUDE, "");
                qdPreferences.setPreference(VERIFIED, false);

                Toast.makeText(context, getResources().getString(R.string.logout), Toast.LENGTH_SHORT).show();

                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;

            default:
                // ignore
                break;
        }

    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {

        mDrawerLayout.closeDrawer(GravityCompat.START);

        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                navigate(menuItem.getItemId());

            }

        }, DRAWER_CLOSE_DELAY_MS);

        return false; // Return true, if you wish to your navigation item as selected.

    }




    // List Adapter for what?
    public class RestoAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        public RestoAdapter() {

            inflater = getLayoutInflater();
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            final ViewHolder holder;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.row_resto, null);

                // set image based on selected text
                holder.textRestoName = (TextView) convertView.findViewById(R.id.textRestoName);
                holder.textAddress = (TextView) convertView.findViewById(R.id.textAddress);
                holder.textDistance = (TextView) convertView.findViewById(R.id.textDistance);
                holder.imgResto = (ImageView) convertView.findViewById(R.id.imgResto);

                convertView.setTag(holder);

            } else {

                holder = (ViewHolder) convertView.getTag();

            }

            holder.textRestoName.setText(restaurantses.get(position).getName());
            holder.textAddress.setText(restaurantses.get(position).getAddress());
            holder.textDistance.setText("< " + restaurantses.get(position).getDistance() + "km");
            Picasso.with(context).load(restaurantses.get(position).getImage()).placeholder(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher).into(holder.imgResto);

            return convertView;

        }

        public class ViewHolder {

            private TextView textRestoName, textDistance, textAddress;
            private ImageView imgResto;

        }

        public int getCount() {

            return restaurantses.size();

        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

    }

    private void onItemsLoadComplete() {

        restoAdapter.notifyDataSetChanged();
        appUtility.hideProgressDialog();
        mapFragment.getMapAsync(this);
        lvRestoName.onLoadMoreComplete();

    }

    // Adjusting map height respectively.
    private void setMapHeight(boolean flag) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        if (flag) {
            ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
            params.height = metrics.heightPixels / 3;
            mapFragment.getView().setLayoutParams(params);
        } else {
            ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
            params.height = metrics.heightPixels;
            mapFragment.getView().setLayoutParams(params);
        }
    }

    // collapse map to its original height
    private void collapse() {

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        fragmentMap.measure(widthSpec, heightSpec);

        ValueAnimator mAnimator = slideAnimator(0, fragmentMap.getMeasuredHeight());
        mAnimator.start();
    }

    // expanding map to full height
    private void expand() {
        int finalHeight = fragmentMap.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);
        mAnimator.start();
    }

    // Value Animator initialization.
    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        return animator;

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        svSearch.onActionViewCollapsed();

        switch (view.getId()) {

            case R.id.imgMenu:

                mDrawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.textFlip:

                fragmentMap.setVisibility(View.VISIBLE);
                if (flipFlag) {

                    flipFlag = false;
                    textFlip.setText(getResources().getString(R.string.text_map));
                    setMapHeight(true);
                    if (isSearchOn) {
                        restaurantses.clear();
                        pageNumber = 1;
                        getAllVenue("");
                    }
                    collapse(); // collapsing map to its previous height.

                } else {

                    flipFlag = true;
                    textFlip.setText(getResources().getString(R.string.text_list));
                    setMapHeight(false);

                    expand(); // expanding map to full height.

                }

                break;

            default:
                break;

        }

    }

}
