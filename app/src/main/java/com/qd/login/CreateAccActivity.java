package com.qd.login;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.qd.R;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class CreateAccActivity extends Activity implements View.OnClickListener,
        ConstantInterface, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Context context;
    private TextView textBack;
    private TextView textHeader;
    private TextView textCreateAcc;
    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtEmail;
    private EditText edtPhone;
    private EditText edtPwd;
    private EditText edtCnfPwd;
    private AppUtility appUtility;
    private String Devicetoken;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private double currLat;
    private double currLong;
    private View mLayout;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_acc);
        initControls();

    }

    @Override
    protected void onStart() {

        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }

    }

    @Override
    protected void onResume() {

        super.onResume();
        appUtility.checkPlayServices(context);

    }

    @Override
    protected void onStop() {

        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 101 && resultCode == RESULT_OK) {

            setResult(RESULT_OK);
            finish();

        }
        if (requestCode == 1000 && resultCode == RESULT_CANCELED) {
            Toast.makeText(context, getResources().getString(R.string.location_off), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            requestLocation();
        }

    }
    // Default android methods end

    // Initializing controls
    private void initControls() {

        mLayout = findViewById(R.id.llView);
        context = CreateAccActivity.this;
        appUtility = ApplicationQd.getInstance().getAppUtility();
        if (!appUtility.checkPlayServices(context)) {
            finish();
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getAccessToken();

        buildGoogleApiClient();

        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getResources().getString(R.string.app_name));
        textCreateAcc = (TextView) findViewById(R.id.textCreateAcc);
        textCreateAcc.setOnClickListener(this);
        edtFirstName = (EditText) findViewById(R.id.edtFirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPhone = (EditText) findViewById(R.id.edtPhone);
        edtPwd = (EditText) findViewById(R.id.edtPwd);
        edtCnfPwd = (EditText) findViewById(R.id.edtCnfPwd);

    }

    // Api call to Register user.
    private void createAcc() {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user[first_name]", edtFirstName.getText().toString().trim());
        hashMap.put("user[last_name]", edtLastName.getText().toString().trim());
        hashMap.put("user[email]", edtEmail.getText().toString().trim());
        hashMap.put("user[password]", edtPwd.getText().toString().trim());
        hashMap.put("user[password_confirmation]", edtCnfPwd.getText().toString().trim());
        hashMap.put("user[phone]", "+91" + edtPhone.getText().toString().trim());
        hashMap.put("user[latitude]", String.valueOf(currLat));
        hashMap.put("user[longitude]", String.valueOf(currLong));
        hashMap.put("user[device_token]", Devicetoken);
        hashMap.put("user[device_type]", "2");

        new CallApiByAsync(BASE_URL + SIGN_UP, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);

                        if (jsonObject.getInt("code") == 200) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("user");

                            QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

                            qdPreferences.setPreference(USER_ID, jsonObject1.getInt(USER_ID));
                            qdPreferences.setPreference(FIRST_NAME, jsonObject1.getString(FIRST_NAME));
                            qdPreferences.setPreference(LAST_NAME, jsonObject1.getString(LAST_NAME));
                            qdPreferences.setPreference(EMAIL, jsonObject1.getString(EMAIL));
                            qdPreferences.setPreference(PHONE, jsonObject1.getString(PHONE));
                            qdPreferences.setPreference(LATITUDE, jsonObject1.getString(LATITUDE));
                            qdPreferences.setPreference(LONGITUDE, jsonObject1.getString(LONGITUDE));

                            onItemsLoadComplete();

                        } else {
                            appUtility.showOkDialog(context, jsonObject.getString("message"));
                            appUtility.hideProgressDialog();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    // Notify adapter, dismiss loader, stop refresh animation
    private void onItemsLoadComplete() {

        appUtility.hideProgressDialog();
        setResult(RESULT_OK);
        finish();

    }

    // Validating input fields.
    private void validate() {

        if (edtFirstName.getText().toString().isEmpty() || edtLastName.getText().toString().isEmpty() || edtPhone.getText().toString().isEmpty() || edtPwd.getText().toString().isEmpty() || edtCnfPwd.getText().toString().isEmpty()) {
            appUtility.showOkDialog(context, getResources().getString(R.string.text_mandetory));
        } else if (appUtility.isValidEmail(edtEmail)) {
            if (edtPwd.getText().toString().length() < 6) {
                appUtility.showOkDialog(context, getResources().getString(R.string.text_pwd_length));
            } else {
                if (edtPwd.getText().toString().equals(edtCnfPwd.getText().toString())) {
                    if (appUtility.isInternetEnable(this)) {
                        appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));
                        createAcc();
                    }
                } else {
                    appUtility.showOkDialog(context, getResources().getString(R.string.text_pwd_match));
                }
            }
        } else {
            appUtility.showOkDialog(context, getResources().getString(R.string.invalid_email));
        }

    }

    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();

    }

    // Checking whether location service is enable or not.
    public void locationChecker(GoogleApiClient mGoogleApiClient, final Activity activity) {
        // Creating location request object
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FATEST_INTERVAL);
        locationRequest.setSmallestDisplacement(DISPLACEMENT);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.e(TAG, "LocationActive");
                        requestLocation();
                        // All location settings are satisfied. The client can initialize location requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                            status.startResolutionForResult(activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                        Log.e(TAG, "Not Active");
                        // Location settings are not satisfied. However, we have no way to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    // Requesting for new location.
    private void requestLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            appUtility.showProgressDialog(this, getResources().getString(R.string.text_location));
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }

    }

    // Getting access token for push notification
    private void getAccessToken() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InstanceID instanceID = InstanceID.getInstance(CreateAccActivity.this);
                    Devicetoken = instanceID.getToken(getString(R.string.gcm_sender_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                    if (!TextUtils.isEmpty(Devicetoken)) {

                        QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
                        qdPreferences.setPreference(ACCESS_TOKEN, Devicetoken);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    // Google api interface abstract methods start
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        // Once connected with google api, get the location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationChecker(googleApiClient, CreateAccActivity.this);
        } else {
            requestLocationPermission();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }
    // Google api interface abstract methods end

    // Location Listener interface abstract method start
    @Override
    public void onLocationChanged(Location location) {
        currLat = location.getLatitude();
        currLong = location.getLongitude();
        appUtility.hideProgressDialog();
    }
    // Location Listener interface abstract method end

    // Requesting Runtime permission
    private void requestLocationPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            AlertDialog.Builder alBuilder = new android.app.AlertDialog.Builder(this);
            alBuilder.setTitle(getResources().getString(R.string.app_name));
            alBuilder.setMessage(getResources().getString(R.string.need_loc_permission));
            alBuilder.setCancelable(false);
            alBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(CreateAccActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
                }
            });
            alBuilder.show();

        } else {
            Snackbar.make(mLayout, getResources().getString(R.string.request_loc_permission), Snackbar.LENGTH_SHORT).show();
            // Request the permission. The result will be received in onRequestPermissionResult().
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            // Request for location permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted.
                Snackbar.make(mLayout, getResources().getString(R.string.permission_granted), Snackbar.LENGTH_SHORT).show();
                locationChecker(googleApiClient, CreateAccActivity.this);
            } else {
                // Permission request was denied.
                Toast.makeText(context, getResources().getString(R.string.permission_not_granted), Toast.LENGTH_SHORT).show();
                finish();
            }
        }

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textBack:

                onBackPressed();
                break;

            case R.id.textCreateAcc:

                validate();
                break;

            default:
                break;

        }

    }
}
