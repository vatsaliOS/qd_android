package com.qd.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.qd.R;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.restaurant.MainActivity;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class LoginActivity extends Activity implements View.OnClickListener, ConstantInterface {

    private TextView textCreateAcc;
    private TextView textSignIn;
    private AppUtility appUtility;
    private Context context;
    private EditText edtEmail;
    private EditText edtPwd;
    private String devicetoken;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initControls();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK) {
            Toast.makeText(context, "User registered successfully", Toast.LENGTH_SHORT).show();
            goToHomePage();
        }
    }

    // Initializing controls
    private void initControls() {

        context = this;
        appUtility = ApplicationQd.getInstance().getAppUtility();
        textCreateAcc = (TextView) findViewById(R.id.textCreateAcc);
        textCreateAcc.setOnClickListener(this);
        textSignIn = (TextView) findViewById(R.id.textSignIn);
        textSignIn.setOnClickListener(this);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPwd = (EditText) findViewById(R.id.edtPwd);

    }

    // Api call to Login user.
    private void login() {

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user[email]", edtEmail.getText().toString().trim());
        hashMap.put("user[password]", edtPwd.getText().toString().trim());
        hashMap.put("user[device_token]", devicetoken);
        hashMap.put("user[device_type]", "2");


        new CallApiByAsync(BASE_URL + SIGN_IN, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);

                        if (jsonObject.getInt("code") == 200) {

                            JSONObject jsonObject1 = jsonObject.getJSONObject("user");


                            QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

                            qdPreferences.setPreference(USER_ID, jsonObject1.getInt(USER_ID));
                            qdPreferences.setPreference(FIRST_NAME, jsonObject1.getString(FIRST_NAME));
                            qdPreferences.setPreference(LAST_NAME, jsonObject1.getString(LAST_NAME));
                            qdPreferences.setPreference(EMAIL, jsonObject1.getString(EMAIL));
                            qdPreferences.setPreference(PHONE, jsonObject1.getString(PHONE));
                            qdPreferences.setPreference(LATITUDE, jsonObject1.getString(LATITUDE));
                            qdPreferences.setPreference(LONGITUDE, jsonObject1.getString(LONGITUDE));


                            onItemsLoadComplete();

                        } else {
                            appUtility.showOkDialog(context, jsonObject.getString("message"));
                            appUtility.hideProgressDialog();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // Notify adapter, dismiss loader, stop refresh animation
    private void onItemsLoadComplete() {

        appUtility.hideProgressDialog();
        Toast.makeText(context, "User signin successfully", Toast.LENGTH_SHORT).show();
        goToHomePage();

    }

    // Validating input fields.
    private void validate() {
        if (appUtility.isValidEmail(edtEmail)) {
            if (!edtPwd.getText().toString().equals("")) {
                if(appUtility.isInternetEnable(this)) {
                    appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));
                    getAccessToken();
                }
            } else {
                appUtility.showOkDialog(context, getResources().getString(R.string.empty_pwd));
            }
        } else {
            appUtility.showOkDialog(context, getResources().getString(R.string.invalid_email));
        }
    }

    // Getting access token for push notification
    private void getAccessToken() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InstanceID instanceID = InstanceID.getInstance(LoginActivity.this);
                    devicetoken = instanceID.getToken(getString(R.string.gcm_sender_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                    if (!TextUtils.isEmpty(devicetoken)) {

                        QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();
                        qdPreferences.setPreference(ACCESS_TOKEN, devicetoken);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                login();
                            }
                        });

                    } else {
                        appUtility.hideProgressDialog();
                        appUtility.showOkDialog(context, getResources().getString(R.string.access_token_fail));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    // go to home page
    private void goToHomePage() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        Intent intent = null;

        switch (view.getId()) {
            case R.id.textCreateAcc:

                intent = new Intent(this, CreateAccActivity.class);
                startActivityForResult(intent, 101);
                break;

            case R.id.textSignIn:

                validate();
                break;

            default:
                break;
        }

    }
}
