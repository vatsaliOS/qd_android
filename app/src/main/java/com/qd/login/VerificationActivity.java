package com.qd.login;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.qd.R;
import com.qd.interfaces.ConstantInterface;
import com.qd.interfaces.OnTaskCompleteListener;
import com.qd.util.AppUtility;
import com.qd.util.ApplicationQd;
import com.qd.util.CallApiByAsync;
import com.qd.util.QdPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by sotsys072 on 4/1/16.
 */
public class VerificationActivity extends Activity implements View.OnClickListener, ConstantInterface {

    private EditText edtConfCode;
    private TextView textCreateAcc;
    private TextView textHeader;
    private AppUtility appUtility;
    private Context context;
    private TextView textBack;
    private TextView textResend;

    // Default android methods start
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        initControls();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // Initializing controls
    private void initControls() {

        edtConfCode = (EditText) findViewById(R.id.edtConfCode);
        textCreateAcc = (TextView) findViewById(R.id.textCreateAcc);
        textCreateAcc.setOnClickListener(this);
        textHeader = (TextView) findViewById(R.id.textHeader);
        textHeader.setText(getResources().getString(R.string.app_name));
        appUtility = ApplicationQd.getInstance().getAppUtility();
        context = this;
        textBack = (TextView) findViewById(R.id.textBack);
        textBack.setOnClickListener(this);
        textResend = (TextView) findViewById(R.id.textResend);
        textResend.setOnClickListener(this);

    }
    // Api call to Verify user.
    private void verifyUser() {

        appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));

        final QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user[user_id]", "" + qdPreferences.getPreference(USER_ID, 0));
        hashMap.put("user[verification_code]", edtConfCode.getText().toString().trim());

        new CallApiByAsync(BASE_URL + VERIFICATION_CODE, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);

                        if (jsonObject.getInt("code") == 200) {

                            qdPreferences.setPreference(VERIFIED, true);
                            onItemsLoadComplete(true);

                        } else {

                            qdPreferences.setPreference(VERIFIED, false);
                            edtConfCode.setText("");
                            onItemsLoadComplete(false);

                        }

                        appUtility.showOkDialog(context, jsonObject.getString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // Notify adapter, dismiss loader, stop refresh animation
    private void onItemsLoadComplete(boolean flag) {

        appUtility.hideProgressDialog();

        if (flag) {

            setResult(RESULT_OK);
            finish();

        }

    }

    // Api call to get Verification code.
    private void getVCode() {

        appUtility.showProgressDialog(this, getResources().getString(R.string.action_loading));

        QdPreferences qdPreferences = ApplicationQd.getInstance().getSharedPreferences();

        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("user[user_id]", "" + qdPreferences.getPreference(USER_ID, 0));

        new CallApiByAsync(BASE_URL + RESEND_VERIFICATION_CODE, hashMap, null, new OnTaskCompleteListener() {
            @Override
            public void onComplete(String resultString) {
                Log.d("log_tag", "testAPI response >> " + resultString);

                if (!resultString.equals("Error")) {
                    try {

                        JSONObject jsonObject = new JSONObject(resultString);
                        appUtility.hideProgressDialog();
                        appUtility.showOkDialog(context, jsonObject.getString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    // All the click listeners
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.textCreateAcc:
                verifyUser();
                break;

            case R.id.textBack:
                onBackPressed();
                break;

            case R.id.textResend:
                getVCode();
                break;

            default:
                break;
        }

    }
}
